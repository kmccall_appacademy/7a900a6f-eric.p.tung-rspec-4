class Book
  # TODO: your code goes here!

  attr_accessor :title
  CAP_EXCEPTIONS = ['the','a','an','for','and','nor','but','and','yet','so','in','of',]

  def title=(title)
    title_arr = title.capitalize.split.map { |word| (CAP_EXCEPTIONS.include?(word) ? word : word.capitalize) }
    @title = title_arr.join(" ")
  end
end
