class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries, :keywords
  def initialize
    @entries = Hash.new
    @keywords = []
  end

  def add(entry)
    @entries[entry] = nil if entry.is_a?(String)
    @entries.merge!(entry) if entry.is_a?(Hash)
    @keywords = @entries.keys
    sort
  end

  def sort
    @entries.sort_by {|k,v| k }
    @keywords.sort!
  end

  def include?(search)
    @keywords.include?(search)
  end

  def find(search)
    search_results = Hash.new
    search_results[search] = @entries[search] if include?(search)
    partial_results = find_partials(search)
    search_results.merge!(partial_results)
  end

  def find_partials(search)
    search_results = Hash.new
    @keywords.each { |word| search_results[word] = @entries[word] if word.include?(search) && word != search}
    search_results
  end

  def printable
    sort
    entries = keywords.map do |keyword|
      %Q{[#{keyword}] "#{@entries[keyword]}"}
    end

    entries.join("\n")
  end

end
