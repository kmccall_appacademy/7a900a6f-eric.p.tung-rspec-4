require 'byebug'
class Temperature
  # TODO: your code goes here!
  attr_reader = :fahrenheit, :celsius, :type
  def initialize(options = {})
    (options[:f].nil? ? @type = :c : @type = :f)
    defaults={f:50, c:50}
    @fahrenheit = options[:f].to_f
    @celsius = options[:c].to_f
  end

  def self.from_celsius(temp_in_c)
    self.new(:c => temp_in_c)
  end

  def self.from_fahrenheit(temp_in_f)
    self.new(:f => temp_in_f)
  end

  def update_temp
    case @type
    when :f
      @celsius = self.ftoc(@fahrenheit)
    when :c
      @fahrenheit = self.ctof(@celsius)
    end
  end

  def in_fahrenheit
    update_temp
    @fahrenheit
  end

  def in_celsius
    update_temp
    @celsius
  end

  def ftoc(temperature)
    (temperature - 32) * 5 / 9
  end

  def ctof(temperature)
    temperature * 9 / 5 + 32
  end
end

class Celsius < Temperature
  def initialize(temperature)
    @celsius = temperature.to_f
    @fahrenheit = self.ctof(@celsius)
  end
end

class Fahrenheit < Temperature
  def initialize(temperature)
    @fahrenheit = temperature.to_f
    @celsius = self.ftoc(@fahrenheit)
  end
end
